package com.stock.mvc.entites;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TemporalType;
import javax.persistence.Temporal;

import java.util.Date;
@Entity
@Table(name="movementsStock")
public class MovementsStock implements Serializable {
	public static final int ENTREE=1;
	public static final int SORTIR=2;
	
	@Id
	@GeneratedValue
	
	private Long idMVTSTK;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateMvt;
	private BigDecimal quantite;
	private int typeMvt;
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	public Long getIdMVTSTK() {
		return idMVTSTK;
	}
	public void setIdMVTSTK(Long idMVTSTK) {
		this.idMVTSTK = idMVTSTK;
	}
	public Date getDateMvt() {
		return dateMvt;
	}
	public void setDateMvt(Date dateMvt) {
		this.dateMvt = dateMvt;
	}
	public BigDecimal getQuantite() {
		return quantite;
	}
	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}
	public int getTypeMvt() {
		return typeMvt;
	}
	public void setTypeMvt(int typeMvt) {
		this.typeMvt = typeMvt;
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	public static int getEntree() {
		return ENTREE;
	}
	public static int getSortir() {
		return SORTIR;
	}
		
		
}
