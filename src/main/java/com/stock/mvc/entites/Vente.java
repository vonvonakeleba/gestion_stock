package com.stock.mvc.entites;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.*;


@Entity
@Table(name="vente")

public class Vente implements Serializable {
	
	@Id
	@GeneratedValue
		
	private Long idVente;
	private String code;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateVente;
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDateVente() {
		return dateVente;
	}

	public void setDateVente(Date dateVente) {
		this.dateVente = dateVente;
	}

	public List<LignedeVente> getLignedeVentes() {
		return lignedeVentes;
	}

	public void setLignedeVentes(List<LignedeVente> lignedeVentes) {
		this.lignedeVentes = lignedeVentes;
	}

	@OneToMany(mappedBy="Vente")
	private List<LignedeVente>lignedeVentes;
	
	
	public Long getIdVente() {
		return idVente;
	}

	public void setIdVente(Long idVente) {
		this.idVente = idVente;
	}

	

}
