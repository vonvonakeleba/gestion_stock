package com.stock.mvc.entites;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="lignecommandefournisseur")
public class LigneCommandeFournisseur implements Serializable {
	
	@Id
	@GeneratedValue
		
	private Long idLigneCommandeFRS;
	@ManyToOne
	@JoinColumn(name="idArticle")
	
	private Article article;
	
	@ManyToOne
	@JoinColumn(name="idCommandeFournisseur")
	private CommandeFournisseur commandeFournisseur;

	public Long getIdLigneCommandeFRS() {
		return idLigneCommandeFRS;
	}

	public void setIdLigneCommandeFRS(Long idLigneCommandeFRS) {
		this.idLigneCommandeFRS = idLigneCommandeFRS;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}
	
		


}
